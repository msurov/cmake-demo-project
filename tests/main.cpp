#include <stdio.h>
#include <gtest/gtest.h>
#include <demolib.h>


TEST(demolib, demofun)
{
    ASSERT_EQ(demofun(5), 15);
}

int main(int argc, char* argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
